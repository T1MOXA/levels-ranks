void NotifClient(int iClient, int iValue, char[] sTitlePhrase)
{
	if(g_bWarmUpCheck && g_iEngineGame == EngineGameCSGO && GameRules_GetProp("m_bWarmupPeriod"))
	{
		return;
	}

	if(iValue != 0 && g_iCountPlayers >= g_iMinimumPlayers && g_bInitialized[iClient])
	{
		if(g_fCoefficient[iClient] > 1.0 && iValue > 0)
		{
			iValue = RoundToNearest(float(iValue) * g_fCoefficient[iClient]);
		}

		g_iExp[iClient] += iValue;
		switch(g_iTypeStatistics)
		{
			case 0: if(g_iExp[iClient] < 0) g_iExp[iClient] = 0;
			case 1: if(g_iExp[iClient] < 500) g_iExp[iClient] = 500;
		}

		CheckRank(iClient);

		if(g_bUsualMessage)
		{
			char sBuffer[64], sMessage[PLATFORM_MAX_PATH];
			FormatEx(sBuffer, sizeof(sBuffer), "%s%i", iValue > 0 ? "+" : "-", iValue > 0 ? iValue : -iValue);
			FormatEx(sMessage, sizeof(sMessage), "%T", sTitlePhrase, iClient, g_iExp[iClient], sBuffer);
			LR_PrintToChat(iClient, "%s", sMessage);
		}
	}
}

void CheckRank(int iClient)
{
	if(iClient && IsClientInGame(iClient) && !IsFakeClient(iClient))
	{
		int iRank = g_iRank[iClient];
		char sMessage[PLATFORM_MAX_PATH];

		if(g_iKills[iClient][0] + g_iDeaths[iClient] >= g_iMinCountKills)
		{
			for(int i = 18; i >= 1; i--)
			{
				if(i == 1)
				{
					g_iRank[iClient] = 1;
				}
				else if(g_iShowExp[i] <= g_iExp[iClient])
				{
					g_iRank[iClient] = i;
					break;
				}
			}
		}

		if(g_iRank[iClient] > iRank)
		{
			FormatEx(sMessage, sizeof(sMessage), "%T", "LevelUp", iClient, g_sShowRank[g_iRank[iClient]]);
			LR_PrintToChat(iClient, "%s", sMessage);
			LR_CallRankForward(iClient, g_iRank[iClient], true);
		}
		else if(g_iRank[iClient] < iRank)
		{
			FormatEx(sMessage, sizeof(sMessage), "%T", "LevelDown", iClient, g_sShowRank[g_iRank[iClient]]);
			LR_PrintToChat(iClient, "%s", sMessage);
			LR_CallRankForward(iClient, g_iRank[iClient], false);
		}
	}
}

public Action PlayTimeCounter(Handle hTimer)
{
	for(int iClient = 1; iClient <= MaxClients; iClient++)
	{
		if(IsClientInGame(iClient) && g_bInitialized[iClient])
		{
			g_iPlayTime[iClient] += 1;
			g_iClientSessionData[iClient][9] += 1;
		}
	}
}

void LR_CallRankForward(int iClient, int iNewLevel, bool bUp)
{
	Call_StartForward(g_hForward_OnLevelChanged);
	Call_PushCell(iClient);
	Call_PushCell(iNewLevel);
	Call_PushCell(bUp);
	Call_Finish();
}