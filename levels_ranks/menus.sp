public Action OnClientSayCommand(int iClient, const char[] command, const char[] sArgs)
{
	if(iClient && g_bInitialized[iClient])
	{
		if(!strcmp(sArgs, "top", false))
		{
			PrintTop(iClient);
		}
		else if(!strcmp(sArgs, "session", false))
		{
			FullMyStatsSession(iClient);
		}
		else if(!strcmp(sArgs, "rank", false))
		{
			char sMessage[PLATFORM_MAX_PATH];
			switch(g_bRankMessage)
			{
				case true:
				{
					for(int i = 1; i <= MaxClients; i++)
					{
						if(g_bInitialized[i])
						{
							FormatEx(sMessage, sizeof(sMessage), "%T", "RankPlayer", i, iClient, g_iDBRankPlayer[iClient], g_iDBCountPlayers, g_iExp[iClient], g_iKills[iClient][0], g_iDeaths[iClient], float(g_iKills[iClient][0] > 0 ? g_iKills[iClient][0] : 1) / float(g_iDeaths[iClient] > 0 ? g_iDeaths[iClient] : 1));
							LR_PrintToChat(i, "%s", sMessage);
						}
					}
				}

				case false:
				{
					FormatEx(sMessage, sizeof(sMessage), "%T", "RankPlayer", iClient, iClient, g_iDBRankPlayer[iClient], g_iDBCountPlayers, g_iExp[iClient], g_iKills[iClient][0], g_iDeaths[iClient], float(g_iKills[iClient][0] > 0 ? g_iKills[iClient][0] : 1) / float(g_iDeaths[iClient] > 0 ? g_iDeaths[iClient] : 1));
					LR_PrintToChat(iClient, "%s", sMessage);
				}
			}
		}
	}

	return Plugin_Continue;
}

public Action ResetSettings(int iClient, int iArgs)
{
	char sMessage[PLATFORM_MAX_PATH];
	SetSettings();
	FormatEx(sMessage, sizeof(sMessage), "%T", "ConfigUpdated", iClient);
	LR_PrintToChat(iClient, "%s", sMessage);
	return Plugin_Handled;
}

public Action ResetStatsFull(int iClient, int iArgs)
{
	ResetStats();
	return Plugin_Handled;
}

public Action ResetStatsPlayer(int iClient, int iArgs)
{
	if(iArgs != 1)
	{
		ReplyToCommand(iClient, "[SM] Usage: sm_lvl_del <steamid>");
		return Plugin_Handled;
	}
	
	if(!g_hDatabase)
	{
		LogLR("ResetStats - database is invalid");
		return Plugin_Handled;
	}
	
	char sArg[65], sQuery[256];
	GetCmdArg(1, sArg, 65);

	char sMessage[PLATFORM_MAX_PATH];
	FormatEx(sMessage, sizeof(sMessage), "%T", "PlayerDataDeleted", iClient, sArg);
	LR_PrintToChat(iClient, "%s", sMessage);

	FormatEx(sQuery, sizeof(sQuery), "DELETE FROM `%s` WHERE steam = '%s';", g_sTableName, sArg);
	SQL_FastQuery(g_hDatabase, sQuery);
	return Plugin_Handled;
}

public Action CallMainMenu(int iClient, int iArgs)
{
	MainMenu(iClient);
	return Plugin_Handled;
}

void MainMenu(int iClient)
{
	char sBuffer[96], sBufferExp[32], sText[128];
	Menu hMenu = new Menu(MainMenuHandler);

	FormatEx(sBuffer, sizeof(sBuffer), "%T", "MainMenu_Rank", iClient, g_sShowRank[g_iRank[iClient]]);
	switch(g_iRank[iClient])
	{
		case 0, 18: FormatEx(sBufferExp, sizeof(sBufferExp), "%i", g_iExp[iClient]);
		default: FormatEx(sBufferExp, sizeof(sBufferExp), "%i / %i", g_iExp[iClient], g_iShowExp[g_iRank[iClient] + 1]);
	}

	hMenu.SetTitle(PLUGIN_NAME ... " " ... PLUGIN_VERSION ... "\n \n%T\n ", "MainMenu_Exp", iClient, sBuffer, sBufferExp, g_iDBRankPlayer[iClient], g_iDBCountPlayers);

	FormatEx(sText, sizeof(sText), "%T", "FullMyStats", iClient); hMenu.AddItem("0", sText);
	FormatEx(sText, sizeof(sText), "%T", "TOP", iClient); hMenu.AddItem("1", sText);
	FormatEx(sText, sizeof(sText), "%T\n ", "AllRanks", iClient); hMenu.AddItem("2", sText);

	if(g_bInventory) {FormatEx(sText, sizeof(sText), "%T", "SettingsLRMenu", iClient); hMenu.AddItem("3", sText);}

	int flags = GetUserFlagBits(iClient);
	if(flags & g_iAdminFlag || flags & ADMFLAG_ROOT)
	{
		FormatEx(sText, sizeof(sText), "%T", "MainAdminMenu", iClient); hMenu.AddItem("4", sText);
	}

	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

MenuLR(MainMenuHandler)
{	
	switch(mAction)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Select:
		{
			char sInfo[2];
			hMenu.GetItem(iSlot, sInfo, sizeof(sInfo));

			switch(StringToInt(sInfo))
			{
				case 0: FullMyStats(iClient);
				case 1: PrintTop(iClient);
				case 2: AllRankMenu(iClient);
				case 3: InventoryMenu(iClient);
				case 4: MainAdminMenu(iClient);
			}
		}
	}
}

void AllRankMenu(int iClient)
{
	char sText[192];
	Menu hMenu = new Menu(AllRankMenuHandler);
	hMenu.SetTitle(PLUGIN_NAME ... " | %T\n ", "AllRanks", iClient);

	for(int i = 1; i <= 18; i++)
	{
		if(i > 1)
		{
			FormatEx(sText, sizeof(sText), "[%i] %s", g_iShowExp[i], g_sShowRank[i]);
			hMenu.AddItem("", sText, ITEMDRAW_DISABLED);
		}
		else
		{
			FormatEx(sText, sizeof(sText), "%s", g_sShowRank[i]);
			hMenu.AddItem("", sText, ITEMDRAW_DISABLED);
		}
	}

	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

MenuLR(AllRankMenuHandler)
{
	switch(mAction)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: if(iSlot == MenuCancel_ExitBack) {MainMenu(iClient);}
	}
}

void InventoryMenu(int iClient)
{
	Menu hMenu = new Menu(MenuHandler_Category);
	hMenu.SetTitle(PLUGIN_NAME ... " | %T\n ", "SettingsLRMenu", iClient);
	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;

	Call_StartForward(g_hForward_OnMenuCreated);
	Call_PushCell(iClient);
	Call_PushCellRef(hMenu);
	Call_Finish();

	if(hMenu.ItemCount == 0)
	{
		hMenu.AddItem("", "-----");
	}

	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

MenuLR(MenuHandler_Category)
{
	switch(mAction)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: if(iSlot == MenuCancel_ExitBack) {MainMenu(iClient);}
		case MenuAction_Select:
		{
			char sInfo[64];
			hMenu.GetItem(iSlot, sInfo, sizeof(sInfo));

			Call_StartForward(g_hForward_OnMenuItemSelected);
			Call_PushCell(iClient);
			Call_PushString(sInfo);
			Call_Finish();
		}
	}

	return 0;
}

void PrintTop(int iClient)
{
	if(g_bInitialized[iClient])
	{
		char sQuery[128];
		FormatEx(sQuery, sizeof(sQuery), g_sSQL_CallTOP, g_sTableName);
		g_hDatabase.Query(SQL_PrintTop, sQuery, iClient);
	}
}

DBCallbackLR(SQL_PrintTop)
{
	if(dbRs == null)
	{
		LogLR("SQL_PrintTop - error while working with data (%s)", sError);
		if(StrContains(sError, "Lost connection to MySQL", false) != -1)
		{
			TryReconnectDB();
		}
		return;
	}

	if(g_bInitialized[iClient])
	{
		int i;
		char sText[256], sName[32], sTemp[512];

		while(dbRs.HasResults && dbRs.FetchRow())
		{
			i++;
			dbRs.FetchString(0, sName, sizeof(sName));
			FormatEx(sText, sizeof(sText), "%d - [ %d ] - %s\n", i, dbRs.FetchInt(1), sName);

			if(strlen(sTemp) + strlen(sText) < 512)
			{
				Format(sTemp, sizeof(sTemp), "%s%s", sTemp, sText);
				sText = "\0";
			}
		}

		Menu hMenu = new Menu(PrintTopMenuHandler);
		hMenu.SetTitle(PLUGIN_NAME ... " | %T\n \n%s\n ", "TOP", iClient, sTemp);

		FormatEx(sText, sizeof(sText), "%T", "BackToMainMenu", iClient);
		hMenu.AddItem("1", sText);

		hMenu.ExitButton = true;
		hMenu.Display(iClient, MENU_TIME_FOREVER);
	}
}

MenuLR(PrintTopMenuHandler)
{
	switch(mAction)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Select: MainMenu(iClient);
	}
}

void FullMyStats(int iClient)
{
	char sText[128];
	Menu hMenu = new Menu(FullStats_Callback);

	int iRoundsAll = g_iRoundWinStats[iClient] + g_iRoundLoseStats[iClient];
	hMenu.SetTitle(PLUGIN_NAME ... " | %T\n ", "FullStats", iClient, g_iPlayTime[iClient] / 3600, g_iPlayTime[iClient] / 60 % 60, g_iPlayTime[iClient] % 60, g_iKills[iClient][0], g_iDeaths[iClient], g_iAssists[iClient], g_iHeadshots[iClient][0], RoundToCeil((100.00 / float(g_iKills[iClient][0] > 0 ? g_iKills[iClient][0] : 1)) * float(g_iHeadshots[iClient][0] > 0 ? g_iHeadshots[iClient][0] : 1)), float(g_iKills[iClient][0] > 0 ? g_iKills[iClient][0] : 1) / float(g_iDeaths[iClient] > 0 ? g_iDeaths[iClient] : 1), g_iShoots[iClient], g_iHits[iClient][0], RoundToCeil((100.00 / float(g_iShoots[iClient] > 0 ? g_iShoots[iClient] : 1)) * float(g_iHits[iClient][0] > 0 ? g_iHits[iClient][0] : 1)), iRoundsAll, g_iRoundWinStats[iClient], RoundToCeil((100.00 / float(iRoundsAll > 0 ? iRoundsAll : 1)) * float(g_iRoundWinStats[iClient] > 0 ? g_iRoundWinStats[iClient] : 1)));

	FormatEx(sText, sizeof(sText), "%T", "FullMyStatsSession", iClient);
	hMenu.AddItem("0", sText);

	if(g_bResetRank)
	{
		FormatEx(sText, sizeof(sText), "%T", "ResetMyStats", iClient);
		hMenu.AddItem("1", sText);
	}

	FormatEx(sText, sizeof(sText), "%T", "BackToMainMenu", iClient);
	hMenu.AddItem("2", sText);

	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

MenuLR(FullStats_Callback)
{
	switch(mAction)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Select:
		{
			char sInfo[2];
			hMenu.GetItem(iSlot, sInfo, sizeof(sInfo));

			switch(StringToInt(sInfo))
			{
				case 0: FullMyStatsSession(iClient);
				case 1: ResetMyStatsMenu(iClient);
				case 2: MainMenu(iClient);
			}
		}
	}
}

void FullMyStatsSession(int iClient)
{
	char sText[128], sBuffer[64];
	Menu hMenu = new Menu(FullStatsSession_Callback);

	int iRoundsAll = g_iClientSessionData[iClient][7] + g_iClientSessionData[iClient][8];
	int iDifference = g_iExp[iClient] - g_iClientSessionData[iClient][0];
	FormatEx(sBuffer, sizeof(sBuffer), "%s%i", iDifference == 0 ? "" : iDifference > 0 ? "+" : "-", iDifference > 0 ? iDifference : -iDifference);

	hMenu.SetTitle(PLUGIN_NAME ... " | %T\n ", "FullStatsSession", iClient, sBuffer, g_iClientSessionData[iClient][9] / 3600, g_iClientSessionData[iClient][9] / 60 % 60, g_iClientSessionData[iClient][9] % 60, g_iClientSessionData[iClient][1], g_iClientSessionData[iClient][2], g_iClientSessionData[iClient][6], g_iClientSessionData[iClient][5], RoundToCeil((100.00 / float(g_iClientSessionData[iClient][1] > 0 ? g_iClientSessionData[iClient][1] : 1)) * float(g_iClientSessionData[iClient][5] > 0 ? g_iClientSessionData[iClient][5] : 1)), float(g_iClientSessionData[iClient][1] > 0 ? g_iClientSessionData[iClient][1] : 1) / float(g_iClientSessionData[iClient][2] > 0 ? g_iClientSessionData[iClient][2] : 1), g_iClientSessionData[iClient][3], g_iClientSessionData[iClient][4], RoundToCeil((100.00 / float(g_iClientSessionData[iClient][3] > 0 ? g_iClientSessionData[iClient][3] : 1)) * float(g_iClientSessionData[iClient][4] > 0 ? g_iClientSessionData[iClient][4] : 1)), iRoundsAll, g_iClientSessionData[iClient][7], RoundToCeil((100.00 / float(iRoundsAll > 0 ? iRoundsAll : 1)) * float(g_iClientSessionData[iClient][7] > 0 ? g_iClientSessionData[iClient][7] : 1)));

	FormatEx(sText, sizeof(sText), "%T", "BackToMainMenu", iClient);
	hMenu.AddItem("", sText);

	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

MenuLR(FullStatsSession_Callback)
{
	switch(mAction)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Select: MainMenu(iClient);
	}
}

void ResetMyStatsMenu(int iClient)
{
	char sText[192];
	Menu hMenu = new Menu(ResetMyStatsMenu_Callback);
	hMenu.SetTitle(PLUGIN_NAME ... " | %T\n ", "ResetMyStatsMenu", iClient);

	FormatEx(sText, sizeof(sText), "%T", "Yes", iClient);
	hMenu.AddItem("", sText);

	FormatEx(sText, sizeof(sText), "%T", "No", iClient);
	hMenu.AddItem("", sText);

	hMenu.ExitButton = false;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

MenuLR(ResetMyStatsMenu_Callback)
{
	switch(mAction)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Select:
		{
			switch(iSlot)
			{
				case 0:
				{
					switch(g_iTypeStatistics)
					{
						case 0: g_iExp[iClient] = 0;
						case 1: g_iExp[iClient] = 1000;
					}

					g_iRank[iClient] = 0;
					g_iKills[iClient][0] = 0;
					g_iHeadshots[iClient][0] = 0;
					g_iHits[iClient][0] = 0;
					g_iDeaths[iClient] = 0;
					g_iShoots[iClient] = 0;
					g_iAssists[iClient] = 0;
					g_iRoundWinStats[iClient] = 0;
					g_iRoundLoseStats[iClient] = 0;
					g_iPlayTime[iClient] = 0;

					CheckRank(iClient);
					MainMenu(iClient);
				}
				case 1: MainMenu(iClient);
			}
		}
	}
}

void MainAdminMenu(int iClient)
{
	char sText[192];
	Menu hMenu = new Menu(MainAdminMenu_Callback);
	hMenu.SetTitle(PLUGIN_NAME ... " | %T\n ", "MainAdminMenu", iClient);

	FormatEx(sText, sizeof(sText), "%T", "ReloadAllConfigs", iClient);
	hMenu.AddItem("0", sText);

	if(g_iTypeStatistics == 0)
	{
		FormatEx(sText, sizeof(sText), "%T", "GiveTakeMenuExp", iClient);
		hMenu.AddItem("1", sText);
	}

	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

MenuLR(MainAdminMenu_Callback)
{
	switch(mAction)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel:
		{
			if(iSlot == MenuCancel_ExitBack)
			{
				MainMenu(iClient);
			}
		}
		case MenuAction_Select:
		{
			char sInfo[2];
			hMenu.GetItem(iSlot, sInfo, sizeof(sInfo));

			switch(StringToInt(sInfo))
			{
				case 0:
				{
					char sMessage[PLATFORM_MAX_PATH];
					SetSettings();
					FormatEx(sMessage, sizeof(sMessage), "%T", "ConfigUpdated", iClient);
					LR_PrintToChat(iClient, "%s", sMessage);
				}
				case 1: GiveTakeValue(iClient);
			}
		}
	}
}

void GiveTakeValue(int iClient)
{
	char sID[16], sNickName[32];
	Menu hMenu = new Menu(GiveTakeValueHandler);
	hMenu.SetTitle(PLUGIN_NAME ... " | %T\n ", "GiveTakeMenuExp", iClient);

	for(int i = 1; i <= MaxClients; i++)
	{
		if(IsClientConnected(i) && IsClientInGame(i) && g_bInitialized[i])
		{
			IntToString(GetClientUserId(i), sID, 16);
			sNickName[0] = '\0';
			GetClientName(i, sNickName, 32);
			hMenu.AddItem(sID, sNickName);
		}
	}
	
	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

MenuLR(GiveTakeValueHandler)
{	
	switch(mAction)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: if(iSlot == MenuCancel_ExitBack) {MainAdminMenu(iClient);}
		case MenuAction_Select:
		{
			char sID[16];
			hMenu.GetItem(iSlot, sID, 16);

			int iRecipient = GetClientOfUserId(StringToInt(sID));
			if(g_bInitialized[iRecipient])
			{
				GiveTakeValueEND(iClient, sID);
			}
			else GiveTakeValue(iClient);
		}
	}
}

public void GiveTakeValueEND(int iClient, char[] sID) 
{
	Menu hMenu = new Menu(ChangeExpPlayersENDHandler);
	hMenu.SetTitle(PLUGIN_NAME ... " | %T\n ", "GiveTakeMenuExp", iClient);
	hMenu.AddItem(sID, "100");
	hMenu.AddItem(sID, "500");
	hMenu.AddItem(sID, "1000");
	hMenu.AddItem(sID, "-1000");
	hMenu.AddItem(sID, "-500");
	hMenu.AddItem(sID, "-100");
	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

MenuLR(ChangeExpPlayersENDHandler)
{	
	switch(mAction)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: if(iSlot == MenuCancel_ExitBack) {GiveTakeValue(iClient);}
		case MenuAction_Select:
		{
			char sInfo[32], sBuffer[32], sBuffer2[PLATFORM_MAX_PATH];
			hMenu.GetItem(iSlot, sInfo, sizeof(sInfo), _, sBuffer, sizeof(sBuffer));
			int iRecipient = GetClientOfUserId(StringToInt(sInfo));
			int iBuffer = StringToInt(sBuffer);

			if(IsClientInGame(iRecipient) && g_bInitialized[iRecipient])
			{
				GiveTakeValueEND(iClient, sInfo);
				g_iExp[iRecipient] += iBuffer;
				if(g_iExp[iRecipient] < 0) g_iExp[iRecipient] = 0;
				CheckRank(iRecipient);

				if(g_bUsualMessage)
				{
					char sMessage[PLATFORM_MAX_PATH];
					FormatEx(sBuffer, sizeof(sBuffer), "%s%i", iBuffer > 0 ? "+" : "-", iBuffer > 0 ? iBuffer : -iBuffer);
					FormatEx(sMessage, sizeof(sMessage), "%T", iBuffer > 0 ? "AdminGive" : "AdminTake", iRecipient, g_iExp[iRecipient], sBuffer);
					LR_PrintToChat(iRecipient, "%s", sMessage);
				}

				FormatEx(sBuffer2, sizeof(sBuffer2), "%s%i", iBuffer > 0 ? "+" : "-", iBuffer > 0 ? iBuffer : -iBuffer);
				LR_PrintToChat(iClient, "%N - {GRAY}%i (%s)", iRecipient, g_iExp[iRecipient], sBuffer2);
			}
		}
	}
}