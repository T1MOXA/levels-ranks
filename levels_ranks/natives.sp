public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	CreateNative("LR_CheckCountPlayers", Native_LR_CheckCountPlayers);
	CreateNative("LR_GetTypeStatistics", Native_LR_GetTypeStatistics);
	CreateNative("LR_GetClientPos", Native_LR_GetClientPos);
	CreateNative("LR_GetClientInfo", Native_LR_GetClientInfo);
	CreateNative("LR_ChangeClientValue", Native_LR_ChangeClientValue);
	CreateNative("LR_SetMultiplierValue", Native_LR_SetMultiplierValue);
	CreateNative("LR_MenuInventory", Native_LR_MenuInventory);
	RegPluginLibrary("levelsranks");
}

public int Native_LR_CheckCountPlayers(Handle hPlugin, int iNumParams)
{
	if(g_iCountPlayers >= g_iMinimumPlayers)
		return true;
	return false;
}

public int Native_LR_GetTypeStatistics(Handle hPlugin, int iNumParams)
{
	return g_iTypeStatistics;
}

public int Native_LR_GetClientPos(Handle hPlugin, int iNumParams)
{
	int iClient = GetNativeCell(1);
	if(g_bInitialized[iClient])
		return g_iDBRankPlayer[iClient];
	return 0;
}

public int Native_LR_GetClientInfo(Handle hPlugin, int iNumParams)
{
	int iClient = GetNativeCell(1);
	int iStats = GetNativeCell(2);

	if(g_bInitialized[iClient])
	{
		switch(iStats)
		{
			case 0: return g_iExp[iClient];
			case 1: return g_iRank[iClient];
			case 2: return g_iKills[iClient][0];
		}
	}

	return 0;
}

public int Native_LR_ChangeClientValue(Handle hPlugin, int iNumParams)
{
	int iClient = GetNativeCell(1);
	int iValue = GetNativeCell(2);

	if(g_bInitialized[iClient])
	{
		g_iExp[iClient] += iValue;
		switch(g_iTypeStatistics)
		{
			case 0: if(g_iExp[iClient] < 0) g_iExp[iClient] = 0;
			case 1: if(g_iExp[iClient] < 500) g_iExp[iClient] = 500;
		}
		CheckRank(iClient);
		return g_iExp[iClient];
	}

	return 0;
}

public int Native_LR_SetMultiplierValue(Handle hPlugin, int iNumParams)
{
	int iClient = GetNativeCell(1);

	if(g_iTypeStatistics == 0 && g_bInitialized[iClient])
	{
		g_fCoefficient[iClient] = GetNativeCell(2);
		return true;
	}

	return false;
}

public int Native_LR_MenuInventory(Handle hPlugin, int iNumParams)
{
	int iClient = GetNativeCell(1);

	if(g_bInitialized[iClient])
	{
		InventoryMenu(iClient);
	}
}